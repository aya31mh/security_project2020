package Server_Client;

import Encryption.AES;
import Encryption.PGP;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.io.*;
import java.net.Socket;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;

public class server implements Runnable {


    private static SecretKey key;
    private static byte[] initVector;
    private static byte[] myVector;

    private static PGP pgp;
    public static PublicKey publicKey;
    public static PrivateKey privateKey;

    public static PublicKey clintPublicKey;
    private static Cipher cipher;
    private static DataOutputStream dOut;
    private static ObjectOutputStream objectOutputStream;
    private static ObjectInputStream objectInputStream;
    private static DataInputStream dIn;
    private static PrintWriter out;

    private Socket socket;
    private AES aes;

    public server(Socket socket) {
        this.socket = socket;
    }

    public static void generatorKeyPair() throws NoSuchAlgorithmException {
        KeyPair keyPair = pgp.generetor();
        privateKey = keyPair.getPrivate();
        publicKey = keyPair.getPublic();
    }

    @Override
    public void run() {
        try {
            System.out.println("Connected: " + socket);

            pgp= new PGP();
            aes = new AES();
            key = aes.createAESKey("aesEncryptionDat");
            myVector = aes.generateVector();

            InputStream s = socket.getInputStream();

            objectInputStream = new ObjectInputStream(s);
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            out = new PrintWriter(socket.getOutputStream(), true);
            dOut = new DataOutputStream(socket.getOutputStream());
            dIn = new DataInputStream(s);
            Scanner in = new Scanner(s);

            //receive session key
//            reciveSessionKey();

            //initialize pair key
            generatorKeyPair();

            //swap public key
            swapPublicKey();

        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Connected: " + socket);

        String fileName = "";
        String contentFile = "";
        boolean done = false;

        try {
            initVector = new byte[16];

            while (!done) {
                byte messageType = dIn.readByte();

                switch (messageType) {
                    case 0: // file Name
                        int l=dIn.readInt();
                        initVector=new byte[l];
                        dIn.read(initVector);
                        initVector = pgp.decryptionVector(initVector,privateKey);
                        int len = dIn.readInt();
                        byte[] FName = new byte[len];
                        dIn.read(FName);
                        fileName = aes.decryption(FName, key, initVector);
                        dOut.writeInt(pgp.encryptionVector(myVector,clintPublicKey).length);
                        dOut.write(pgp.encryptionVector(myVector,clintPublicKey));
                        dOut.writeInt(aes.encryption("file name send successfully", key, myVector).length);
                        dOut.write(aes.encryption("file name send successfully", key, myVector));
                        break;
                    case 1: // View Action
                        File file = new File(fileName);
                        if (file.createNewFile()) {
                            System.out.println("create new");
                            dOut.writeInt(pgp.encryptionVector(myVector,clintPublicKey).length);
                            dOut.write(pgp.encryptionVector(myVector,clintPublicKey));
                            dOut.writeInt(aes.encryption("File isn't exist but it created", key, myVector).length);
                            dOut.write(aes.encryption("File isn't exist but it created", key, myVector));
                        } else {
                            // file is exist
                            System.out.println("file is exist");
                            Scanner sc = new Scanner(file);//file to be scanned
                            while (sc.hasNextLine())        //returns true if and only if scanner has another token
                                contentFile += sc.nextLine() + "\n";

                            System.out.println(contentFile);
                            dOut.writeInt(pgp.encryptionVector(myVector,clintPublicKey).length);
                            dOut.write(pgp.encryptionVector(myVector,clintPublicKey));
                            dOut.writeInt(aes.encryption(contentFile, key, myVector).length);
                            dOut.write(aes.encryption(contentFile, key, myVector));
                        }

                        break;
                    case 2: // Edit Action
                        File file1 = new File(fileName);

                        l=dIn.readInt();
                        initVector=new byte[l];
                        dIn.read(initVector);
                        initVector = pgp.decryptionVector(initVector,privateKey);
                        len = dIn.readInt();
                        byte[] res = new byte[len];
                        dIn.read(res);

                        contentFile = aes.decryption(res, key, initVector);
                        if (file1.createNewFile()) {
                            FileWriter myWriter = new FileWriter(fileName);
                            myWriter.write(contentFile);
                            myWriter.close();
                        } else {
                            System.out.println("File already exists.");
                            FileWriter myWriter = new FileWriter(fileName);
                            myWriter.write(contentFile);
                            myWriter.close();
                        }
                        dOut.writeInt(pgp.encryptionVector(myVector,clintPublicKey).length);
                        dOut.write(pgp.encryptionVector(myVector,clintPublicKey));
                        dOut.writeInt(aes.encryption("Edit File is Successfull", key, myVector).length);
                        dOut.write(aes.encryption("Edit File is Successfull", key, myVector));
                        break;
                    default:
                        done = true;
                }
            }

            dIn.close();
        } catch (Exception e) {
            System.out.println("Error:" + socket);
            System.out.println(e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("An Error occurred");
            }
            System.out.println("Closed: " + socket);
        }
    }

//    private void reciveSessionKey() throws IOException {
//        key = dIn.readUTF();
//        out.println("true");
//    }

    private void swapPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, ClassNotFoundException {
        //receive public key (key's server)

        PK pk = (PK) objectInputStream.readObject();

        byte[] pubKey = pk.data;
        X509EncodedKeySpec ks = new X509EncodedKeySpec(pubKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        clintPublicKey = kf.generatePublic(ks);

        //send public key(key's clint)
        PK pkObject = new PK(publicKey.getEncoded());
        pkObject.data = publicKey.getEncoded();
        objectOutputStream.writeObject(pkObject);

    }


}
