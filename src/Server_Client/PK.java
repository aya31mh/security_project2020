package Server_Client;

import java.io.Serializable;

public class PK implements Serializable {
    //    PublicKey publicKey;
    byte[] data;

    public PK(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }


}
