package Server_Client;

import Encryption.AES;
import Encryption.PGP;

import javax.crypto.SecretKey;
import java.io.*;
import java.net.Socket;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;

public class client {
    private static SecretKey key;
    private static byte[] serverInitVector;
    private static byte[] myVector;

    private static PGP pgp;
    public static PublicKey publicKey;
    public static PrivateKey privateKey;
    public static PublicKey serverPublicKey;

    private static ObjectOutputStream objectOutputStream;
    private static ObjectInputStream objectInputStream;

    private static DataOutputStream dOut;
    private static DataInputStream dIn;

    public static void generatorKeyPair() throws NoSuchAlgorithmException {
        KeyPair keyPair = pgp.generetor();
        privateKey = keyPair.getPrivate();
        publicKey = keyPair.getPublic();
    }

    public static void main(String[] args) throws Exception {

        AES aes = new AES();
        key = aes.createAESKey("aesEncryptionDat");
        myVector = aes.generateVector();

        //initialize pair key
        pgp = new PGP();
        generatorKeyPair();

        try (Socket socket = new Socket("127.0.0.1", 11111)) {
            //My Code
            Scanner scanner = new Scanner(System.in);
            Scanner in = new Scanner(socket.getInputStream());
            dOut = new DataOutputStream(socket.getOutputStream());
            dIn = new DataInputStream(socket.getInputStream());
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectInputStream = new ObjectInputStream(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            serverInitVector = new byte[16];

            //send session key
//            sendSessionKey();

            //send public key(key's clint)
            swapPublicKey();
            System.out.println("MMM");
            //encryption myVector by public key
            byte[] myVectorEncrypted = pgp.encryptionVector(myVector, serverPublicKey);

            // Send File Name
            System.out.println("Enter File Name then Ctrl+D or Ctrl+C to quit :");
            String fileName = scanner.nextLine();
            dOut.writeByte(0);
//            dOut.write(myVector);
            dOut.writeInt(myVectorEncrypted.length);
            dOut.write(myVectorEncrypted);
            dOut.writeInt(aes.encryption(fileName, key, myVector).length);
            dOut.write(aes.encryption(fileName, key, myVector));
            dOut.flush();

            int l = dIn.readInt();
            serverInitVector = new byte[l];
            dIn.read(serverInitVector);
            serverInitVector = pgp.decryptionVector(serverInitVector, privateKey);
            int len = dIn.readInt();
            byte[] res = new byte[len];
            dIn.read(res);
            System.out.println("server response = " + aes.decryption(res, key, serverInitVector));

            System.out.println("Enter your choice : \n  1.View content file.\n  2.Edit content file.\n");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    // Send view Action
                    dOut.writeByte(1);
                    l = dIn.readInt();
                    serverInitVector = new byte[l];
                    dIn.read(serverInitVector);
                    serverInitVector = pgp.decryptionVector(serverInitVector, privateKey);
                    len = dIn.readInt();
                    res = new byte[len];
                    dIn.read(res);
                    System.out.println("Server response : " + aes.decryption(res, key, serverInitVector));

                    break;
                case 2:
                    // Send Edit Action
                    System.out.println("Enter content of file then Ctrl+D or Ctrl+C to quit");
                    String content = "";

                    while (scanner.hasNextLine()) {
                        content += scanner.nextLine() + "\n";
                    }
                    dOut.writeByte(2);
                    dOut.writeInt(myVectorEncrypted.length);
                    dOut.write(myVectorEncrypted);
                    dOut.writeInt(aes.encryption(content, key, myVector).length);
                    dOut.write(aes.encryption(content, key, myVector));

                    l = dIn.readInt();
                    serverInitVector = new byte[l];
                    dIn.read(serverInitVector);
                    serverInitVector = pgp.decryptionVector(serverInitVector, privateKey);
                    len = dIn.readInt();
                    res = new byte[len];
                    dIn.read(res);
                    System.out.println("Server response : " + aes.decryption(res, key, serverInitVector));

                    break;
                default:
                    break;
            }

            // Send the exit message
            dOut.writeByte(-1);
            dOut.flush();

            dOut.close();

        } catch (Exception e) {
            System.out.println("!!!!!!!!!!!!");
            System.out.println(e);
        }
    }

    private static void swapPublicKey() throws IOException, ClassNotFoundException, NoSuchAlgorithmException, InvalidKeySpecException {
        //send client public key
        PK pkObject = new PK(publicKey.getEncoded());

        pkObject.data = publicKey.getEncoded();
        objectOutputStream.writeObject(pkObject);


        //receive server public key

        PK pk = (PK) objectInputStream.readObject();
        byte[] pubKey = pk.data;
        X509EncodedKeySpec ks = new X509EncodedKeySpec(pubKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");

        serverPublicKey = kf.generatePublic(ks);

    }

//    private static void sendSessionKey() throws IOException {
//        //send Key
//        dOut.writeUTF(key.getAlgorithm());
//        dOut.flush();
//
//        System.out.println("Waiting for the session to be accepted");
//        //recive server response
//        String bb = in.nextLine();
//        System.out.println("BBB "+bb);
//        if (!bb.equals("true"))
//            return;
//
//    }


}
