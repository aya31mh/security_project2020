package Server_Client;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class multiThreading {
    public static void main(String[] args) throws IOException {
        try (ServerSocket listener = new ServerSocket(11111)) {
            System.out.println("The files server is running...");
            ExecutorService pool = Executors.newFixedThreadPool(20);
            while (true) {
                pool.execute(new server(listener.accept()));
            }
        }
    }
}
